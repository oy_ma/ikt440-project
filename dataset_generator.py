from tensorflow.python.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from shutil import rmtree
import os
import sys

train_idg = ImageDataGenerator(
    rotation_range=5,
    width_shift_range=0.05,
    height_shift_range=0.05,
    shear_range=0.05,
    zoom_range=0.05,
    horizontal_flip=False,
    cval=0,
    fill_mode='constant'
)

test_idg = ImageDataGenerator(

)


def create_training_data(data_dir):
    training_amount = 32
    testing_amount = 1
    testing_portion = 1
    min_per_subject = 2  # Exclude subjects with available images < min. May screw things up if including both sides.

    progress = 0

    set_path = 'aug_data'
    if os.path.exists(set_path):
        rmtree(set_path, ignore_errors=True)

    subjects = os.listdir(data_dir)

    for subject in subjects:
        train_path = set_path + '/training/' + subject + '/'
        test_path = set_path + '/testing/' + subject + '/'

        for side in os.listdir(data_dir + '/' + subject)[:1]:
            teeth = os.listdir(data_dir + '/' + subject + '/' + side)
            if len(teeth) < min_per_subject:
                break
            if not os.path.exists(train_path):
                os.makedirs(train_path)
            if not os.path.exists(test_path):
                os.makedirs(test_path)
            # training_amount = len(teeth) - testing_amount  # all images in current folder except training amount
            for i in range(training_amount):
                index = 0
                if len(teeth) > 1:
                    index = i % (len(teeth) - testing_portion)
                img = load_img(data_dir + '/' + subject + '/' + side + '/' + teeth[index])
                x = img_to_array(img)
                x = x.reshape((1,) + x.shape)
                for _ in train_idg.flow(x, batch_size=1, save_to_dir=train_path, save_prefix='teeth',
                                        save_format='bmp'):
                    break
            for j in range(testing_amount):
                img = load_img(data_dir + '/' + subject + '/' + side + '/' + teeth[len(teeth) - testing_portion +
                                                                                   (j % testing_portion)])
                x = img_to_array(img)
                x = x.reshape((1,) + x.shape)
                for _ in test_idg.flow(x, batch_size=1, save_to_dir=test_path, save_prefix='teeth',
                                       save_format='bmp'):
                    break
        progress += 1
        sys.stdout.write('\r' + '(' + str(round((progress / len(subjects)) * 100, 1)) + '%)')


create_training_data('radiograph/dataset/experimentset')

