import tensorflow as tf
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.layers import Dropout, Flatten, Dense
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras import backend as kb
from tensorflow.python.keras import callbacks
from tensorflow.python.keras.preprocessing.image import load_img, img_to_array
from tensorflow.python.keras.optimizers import RMSprop
import numpy as np
import os
import matplotlib.pyplot as plt
from random import randint


best_val_acc = 0


class CustomCallback(callbacks.Callback):
    def on_train_begin(self, logs=None):
        self.x = []
        self.loss = []
        self.val_loss = []
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(111)
        self.ax.plot(100, 0, 'r-', label='loss')
        self.ax.plot(100, 0, 'b-', label='val_loss')
        self.ax.legend(loc='upper right')
        plt.ion()
        plt.show()

    def on_epoch_end(self, epoch, logs=None):
        plt.pause(0.01)  # prevent plot from freezing during training
        self.x.append(epoch+1)
        self.loss.append(logs.get('loss'))
        self.val_loss.append(logs.get('val_loss'))
        self.ax.plot(self.x, self.loss, 'r-', label='loss')
        self.ax.plot(self.x, self.val_loss, 'b-', label='val_loss')
        self.figure.canvas.draw()
        global best_val_acc
        if logs.get('val_acc') > best_val_acc:
            best_val_acc = logs.get('val_acc')

    def on_train_end(self, logs=None):
        plt.savefig('plots/plot.png')


def load_image(file, size_x, size_y):
    img = load_img(file, target_size=(size_y, size_x))
    return img_to_array(img)


def train_network(directory):
    train_images = []
    train_labels = []
    valid_images = []
    valid_labels = []

    image_size_x = 150  # size of loaded images (resized)
    # image_size_y = int(image_size_x/1.33)
    image_size_y = image_size_x
    epochs = 100
    batch_size = 32

    all_data = os.listdir(directory)

    subjects = []

    for d in all_data:
        if d == 'training':
            subjects = os.listdir(directory+'/'+d)
            print("Training subjects:", len(subjects))
            i = 0
            for s in subjects:
                teeth = os.listdir(directory+'/'+d+'/'+s)
                for tooth in teeth:
                    data = load_image(directory+'/'+d+'/'+s+'/'+tooth, image_size_x, image_size_y)
                    train_images.append(data)
                    train_labels.append(np.eye(len(subjects))[i])
                i += 1
        elif d == 'testing':
            subjects = os.listdir(directory+'/'+d)
            print("Testing subjects:", len(subjects))
            i = 0
            for s in subjects:
                teeth = os.listdir(directory+'/'+d+'/'+s)
                for tooth in teeth:
                    data = load_image(directory+'/'+d+'/'+s+'/'+tooth, image_size_x, image_size_y)
                    valid_images.append(data)
                    valid_labels.append(np.eye(len(subjects))[i])
                i += 1

    print('training images:', len(train_images))
    print('validation images:', len(valid_images))
    print('total:', len(train_images+valid_images))
    print('image size: {:d}x{:d}'.format(image_size_x, image_size_y))

    train_x = np.array(train_images)
    train_x = train_x.astype('float32')
    train_x /= 255
    train_y = np.array(train_labels)
    valid_x = np.array(valid_images)
    valid_x = valid_x.astype('float32')
    valid_x /= 255
    valid_y = np.array(valid_labels)

    # plt.imshow(train_x[randint(0, len(train_images)-1)])  # preview random sample image
    # plt.show()

    # shape of the numpy array containing training data
    if kb.image_data_format() == 'channels_first':
        input_shape = (3, image_size_y, image_size_x)
    else:
        input_shape = (image_size_y, image_size_x, 3)

    print("input shape:", input_shape)

    # set up network
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.60))
    model.add(Dense(len(subjects), activation='softmax'))

    optimizer = RMSprop(lr=0.0001)

    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    model.summary()

    earlystopping = callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=0, mode='auto')

    checkpointer = callbacks.ModelCheckpoint(filepath='tmp/weights.hdf5', verbose=1, save_best_only=True)

    # tensorboard = callbacks.TensorBoard(log_dir='./graph', histogram_freq=1, batch_size=32, write_graph=True,
    #                                    write_grads=False, write_images=False)

    cc = CustomCallback()

    # run model
    with tf.device('/gpu:0'):
        model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, validation_data=(valid_x, valid_y),
                  callbacks=[checkpointer, earlystopping, cc])

    print("\nBest val_loss: ", checkpointer.best)
    print("Best val_acc: ", best_val_acc)


train_network("aug_data")
input("Press enter to exit program")
